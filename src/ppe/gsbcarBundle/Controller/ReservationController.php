<?php

namespace ppe\gsbcarBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ReservationController extends Controller
{
    /**
     * @Route("/reserver/")
     */
    public function reserverAction()
    {
        return $this->render('ppegsbcarBundle:Default:reservationForm.html.twig');
    }
}