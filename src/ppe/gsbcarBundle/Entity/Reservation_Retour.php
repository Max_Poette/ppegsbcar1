<?php

namespace ppe\gsbcarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservation_Retour
 *
 * @ORM\Table(name="reservation__retour")
 * @ORM\Entity(repositoryClass="ppe\gsbcarBundle\Repository\Reservation_RetourRepository")
 */
class Reservation_Retour
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="Kilometrage", type="integer")
     */
    private $kilometrage;

    /**
     * @var string
     *
     * @ORM\Column(name="Etat", type="string", length=255)
     */
    private $etat;

    /**
    * @ORM\ManyToOne(targetEntity="ppe\gsbcarBundle\Entity\Reservation")
    * @ORM\JoinColumn(nullable=false)
    */
    private $laReservation;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kilometrage
     *
     * @param integer $kilometrage
     * @return Reservation_Retour
     */
    public function setKilometrage($kilometrage)
    {
        $this->kilometrage = $kilometrage;

        return $this;
    }

    /**
     * Get kilometrage
     *
     * @return integer 
     */
    public function getKilometrage()
    {
        return $this->kilometrage;
    }

    /**
     * Set etat
     *
     * @param string $etat
     * @return Reservation_Retour
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set laReservation
     *
     * @param \ppe\gsbcarBundle\Entity\Reservation $laReservation
     * @return Reservation_Retour
     */
    public function setLaReservation(\ppe\gsbcarBundle\Entity\Reservation $laReservation)
    {
        $this->laReservation = $laReservation;

        return $this;
    }

    /**
     * Get laReservation
     *
     * @return \ppe\gsbcarBundle\Entity\Reservation 
     */
    public function getLaReservation()
    {
        return $this->laReservation;
    }
}
