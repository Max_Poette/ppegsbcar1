<?php

namespace ppe\gsbcarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservation
 *
 * @ORM\Table(name="reservation")
 * @ORM\Entity(repositoryClass="ppe\gsbcarBundle\Repository\ReservationRepository")
 */
class Reservation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut", type="datetime")
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin", type="datetime")
     */
    private $dateFin;

    /**
     * @var string
     *
     * @ORM\Column(name="destination", type="string", length=255)
     */
    private $destination;

    /**
    * @ORM\ManyToOne(targetEntity="ppe\gsbcarBundle\Entity\Thermique")
    * @ORM\JoinColumn(nullable =true)
    */
    private $vThermique;
    
    /**
    * @ORM\ManyToOne(targetEntity="ppe\gsbcarBundle\Entity\Electrique")
    * @ORM\JoinColumn(nullable =true)
    */
    private $vElectrique;
    
    /**
    * @ORM\ManyToOne(targetEntity="ppe\gsbcarBundle\Entity\User")
    * @ORM\JoinColumn(nullable=false)
    */
    private $user;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     * @return Reservation
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime 
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     * @return Reservation
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime 
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set destination
     *
     * @param string $destination
     * @return Reservation
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * Get destination
     *
     * @return string 
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Set vThermique
     *
     * @param \ppe\gsbcarBundle\Entity\Thermique $vThermique
     * @return Reservation
     */
    public function setVThermique(\ppe\gsbcarBundle\Entity\Thermique $vThermique = null)
    {
        $this->vThermique = $vThermique;

        return $this;
    }

    /**
     * Get vThermique
     *
     * @return \ppe\gsbcarBundle\Entity\Thermique 
     */
    public function getVThermique()
    {
        return $this->vThermique;
    }

    /**
     * Set vElectrique
     *
     * @param \ppe\gsbcarBundle\Entity\Electrique $vElectrique
     * @return Reservation
     */
    public function setVElectrique(\ppe\gsbcarBundle\Entity\Electrique $vElectrique = null)
    {
        $this->vElectrique = $vElectrique;

        return $this;
    }

    /**
     * Get vElectrique
     *
     * @return \ppe\gsbcarBundle\Entity\Electrique 
     */
    public function getVElectrique()
    {
        return $this->vElectrique;
    }

    /**
     * Set user
     *
     * @param \ppe\gsbcarBundle\Entity\User $user
     * @return Reservation
     */
    public function setUser(\ppe\gsbcarBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ppe\gsbcarBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
