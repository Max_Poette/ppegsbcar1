<?php

namespace ppe\gsbcarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Revision
 *
 * @ORM\Table(name="revision")
 * @ORM\Entity
 */
class Revision
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
  
    /**
    * @ORM\ManyToOne(targetEntity="ppe\gsbcarBundle\Entity\Thermique")
    * @ORM\JoinColumn()
    */
    private $vThermique;
    
    /**
    * @ORM\ManyToOne(targetEntity="ppe\gsbcarBundle\Entity\Electrique")
    * @ORM\JoinColumn()
    */
    private $vElectrique;
    
    /**
     * @var string
     *
     * @ORM\Column(name="date_debut", type="string", length=50, nullable=false)
     */
    private $dateDebut;

    /**
     * @var string
     *
     * @ORM\Column(name="date_fin", type="string", length=50, nullable=false)
     */
    private $dateFin;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateDebut
     *
     * @param string $dateDebut
     * @return Revision
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return string 
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param string $dateFin
     * @return Revision
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return string 
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set vThermique
     *
     * @param \ppe\gsbcarBundle\Entity\Thermique $vThermique
     * @return Revision
     */
    public function setVThermique(\ppe\gsbcarBundle\Entity\Thermique $vThermique = null)
    {
        $this->vThermique = $vThermique;

        return $this;
    }

    /**
     * Get vThermique
     *
     * @return \ppe\gsbcarBundle\Entity\Thermique 
     */
    public function getVThermique()
    {
        return $this->vThermique;
    }

    /**
     * Set vElectrique
     *
     * @param \ppe\gsbcarBundle\Entity\Electrique $vElectrique
     * @return Revision
     */
    public function setVElectrique(\ppe\gsbcarBundle\Entity\Electrique $vElectrique = null)
    {
        $this->vElectrique = $vElectrique;

        return $this;
    }

    /**
     * Get vElectrique
     *
     * @return \ppe\gsbcarBundle\Entity\Electrique 
     */
    public function getVElectrique()
    {
        return $this->vElectrique;
    }
}
