<?php

namespace ppe\gsbcarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Frais_Reservation
 *
 * @ORM\Table(name="frais__reservation")
 * @ORM\Entity(repositoryClass="ppe\gsbcarBundle\Repository\Frais_ReservationRepository")
 */
class Frais_Reservation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var float
     *
     * @ORM\Column(name="Montant", type="float")
     */
    private $montant;

    /**
    * @ORM\ManyToOne(targetEntity="ppe\gsbcarBundle\Entity\Reservation")
    * @ORM\JoinColumn(nullable=false)
    */
    private $reservation;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Frais_Reservation
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set montant
     *
     * @param float $montant
     * @return Frais_Reservation
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return float 
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set reservation
     *
     * @param \ppe\gsbcarBundle\Entity\Reservation $reservation
     * @return Frais_Reservation
     */
    public function setReservation(\ppe\gsbcarBundle\Entity\Reservation $reservation)
    {
        $this->reservation = $reservation;

        return $this;
    }

    /**
     * Get reservation
     *
     * @return \ppe\gsbcarBundle\Entity\Reservation 
     */
    public function getReservation()
    {
        return $this->reservation;
    }
}
