<?php

namespace ppe\gsbcarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Electrique
 *
 * @ORM\Table(name="electrique")
 * @ORM\Entity
 */

class Electrique
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Immatriculation", type="string", length=15, nullable=false)
     */
    private $immatriculation;

    /**
     * @var string
     *
     * @ORM\Column(name="Date_achat", type="string", length=20, nullable=false)
     */
    private $dateAchat;

    /**
     * @var string
     *
     * @ORM\Column(name="Date_circulation", type="string", length=20, nullable=false)
     */
    private $dateCirculation;

    /**
     * @var integer
     *
     * @ORM\Column(name="Intervalle_revision", type="integer", nullable=false)
     */
    private $intervalleRevision;

    /**
     * @var integer
     *
     * @ORM\Column(name="Kilometrage", type="integer", nullable=false)
     */
    private $kilometrage;

    /**
     * @var string
     *
     * @ORM\Column(name="Marque", type="string", length=20, nullable=false)
     */
    private $marque;

    /**
     * @var string
     *
     * @ORM\Column(name="Modele", type="string", length=20, nullable=false)
     */
    private $modele;

    /**
     * @var integer
     *
     * @ORM\Column(name="Prix_achat", type="integer", nullable=false)
     */
    private $prixAchat;

    /**
     * @var integer
     *
     * @ORM\Column(name="Prix_loyer", type="integer", nullable=false)
     */
    private $prixLoyer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Utilisable", type="boolean", nullable=false)
     */
    private $utilisable;

    /**
     * @var integer
     *
     * @ORM\Column(name="Puissance", type="integer", nullable=false)
     */
    private $puissance;

    /**
     * @var integer
     *
     * @ORM\Column(name="Derniere_revision", type="integer", nullable=false)
     */
    private $derniereRevision;

    /**
     * @var integer
     *
     * @ORM\Column(name="Autonomie", type="integer", nullable=false)
     */
    private $autonomie;

    /**
     * @var integer
     *
     * @ORM\Column(name="Temps_charge", type="integer", nullable=false)
     */
    private $tempsCharge;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set immatriculation
     *
     * @param string $immatriculation
     * @return Electrique
     */
    public function setImmatriculation($immatriculation)
    {
        $this->immatriculation = $immatriculation;

        return $this;
    }

    /**
     * Get immatriculation
     *
     * @return string 
     */
    public function getImmatriculation()
    {
        return $this->immatriculation;
    }

    /**
     * Set dateAchat
     *
     * @param string $dateAchat
     * @return Electrique
     */
    public function setDateAchat($dateAchat)
    {
        $this->dateAchat = $dateAchat;

        return $this;
    }

    /**
     * Get dateAchat
     *
     * @return string 
     */
    public function getDateAchat()
    {
        return $this->dateAchat;
    }

    /**
     * Set dateCirculation
     *
     * @param string $dateCirculation
     * @return Electrique
     */
    public function setDateCirculation($dateCirculation)
    {
        $this->dateCirculation = $dateCirculation;

        return $this;
    }

    /**
     * Get dateCirculation
     *
     * @return string 
     */
    public function getDateCirculation()
    {
        return $this->dateCirculation;
    }

    /**
     * Set intervalleRevision
     *
     * @param integer $intervalleRevision
     * @return Electrique
     */
    public function setIntervalleRevision($intervalleRevision)
    {
        $this->intervalleRevision = $intervalleRevision;

        return $this;
    }

    /**
     * Get intervalleRevision
     *
     * @return integer 
     */
    public function getIntervalleRevision()
    {
        return $this->intervalleRevision;
    }

    /**
     * Set kilometrage
     *
     * @param integer $kilometrage
     * @return Electrique
     */
    public function setKilometrage($kilometrage)
    {
        $this->kilometrage = $kilometrage;

        return $this;
    }

    /**
     * Get kilometrage
     *
     * @return integer 
     */
    public function getKilometrage()
    {
        return $this->kilometrage;
    }

    /**
     * Set marque
     *
     * @param string $marque
     * @return Electrique
     */
    public function setMarque($marque)
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * Get marque
     *
     * @return string 
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * Set modele
     *
     * @param string $modele
     * @return Electrique
     */
    public function setModele($modele)
    {
        $this->modele = $modele;

        return $this;
    }

    /**
     * Get modele
     *
     * @return string 
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * Set prixAchat
     *
     * @param integer $prixAchat
     * @return Electrique
     */
    public function setPrixAchat($prixAchat)
    {
        $this->prixAchat = $prixAchat;

        return $this;
    }

    /**
     * Get prixAchat
     *
     * @return integer 
     */
    public function getPrixAchat()
    {
        return $this->prixAchat;
    }

    /**
     * Set prixLoyer
     *
     * @param integer $prixLoyer
     * @return Electrique
     */
    public function setPrixLoyer($prixLoyer)
    {
        $this->prixLoyer = $prixLoyer;

        return $this;
    }

    /**
     * Get prixLoyer
     *
     * @return integer 
     */
    public function getPrixLoyer()
    {
        return $this->prixLoyer;
    }

    /**
     * Set utilisable
     *
     * @param boolean $utilisable
     * @return Electrique
     */
    public function setUtilisable($utilisable)
    {
        $this->utilisable = $utilisable;

        return $this;
    }

    /**
     * Get utilisable
     *
     * @return boolean 
     */
    public function getUtilisable()
    {
        return $this->utilisable;
    }

    /**
     * Set puissance
     *
     * @param integer $puissance
     * @return Electrique
     */
    public function setPuissance($puissance)
    {
        $this->puissance = $puissance;

        return $this;
    }

    /**
     * Get puissance
     *
     * @return integer 
     */
    public function getPuissance()
    {
        return $this->puissance;
    }

    /**
     * Set derniereRevision
     *
     * @param integer $derniereRevision
     * @return Electrique
     */
    public function setDerniereRevision($derniereRevision)
    {
        $this->derniereRevision = $derniereRevision;

        return $this;
    }

    /**
     * Get derniereRevision
     *
     * @return integer 
     */
    public function getDerniereRevision()
    {
        return $this->derniereRevision;
    }

    /**
     * Set autonomie
     *
     * @param integer $autonomie
     * @return Electrique
     */
    public function setAutonomie($autonomie)
    {
        $this->autonomie = $autonomie;

        return $this;
    }

    /**
     * Get autonomie
     *
     * @return integer 
     */
    public function getAutonomie()
    {
        return $this->autonomie;
    }

    /**
     * Set tempsCharge
     *
     * @param integer $tempsCharge
     * @return Electrique
     */
    public function setTempsCharge($tempsCharge)
    {
        $this->tempsCharge = $tempsCharge;

        return $this;
    }

    /**
     * Get tempsCharge
     *
     * @return integer 
     */
    public function getTempsCharge()
    {
        return $this->tempsCharge;
    }
}
