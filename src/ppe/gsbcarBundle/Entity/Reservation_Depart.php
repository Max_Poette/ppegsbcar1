<?php

namespace ppe\gsbcarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservation_Depart
 *
 * @ORM\Table(name="reservation__depart")
 * @ORM\Entity(repositoryClass="ppe\gsbcarBundle\Repository\Reservation_DepartRepository")
 */
class Reservation_Depart
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Etat", type="string", length=50)
     */
    private $etat;

    /**
     * @var int
     *
     * @ORM\Column(name="Kilometrage", type="integer")
     */
    private $kilometrage;

    /**
    * @ORM\ManyToOne(targetEntity="ppe\gsbcarBundle\Entity\Reservation")
    * @ORM\JoinColumn(nullable=false)
    */
    private $laReservation;

       
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set etat
     *
     * @param string $etat
     * @return Reservation_Depart
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set kilometrage
     *
     * @param integer $kilometrage
     * @return Reservation_Depart
     */
    public function setKilometrage($kilometrage)
    {
        $this->kilometrage = $kilometrage;

        return $this;
    }

    /**
     * Get kilometrage
     *
     * @return integer 
     */
    public function getKilometrage()
    {
        return $this->kilometrage;
    }

    /**
     * Set laReservation
     *
     * @param \ppe\gsbcarBundle\Entity\Reservation $laReservation
     * @return Reservation_Depart
     */
    public function setLaReservation(\ppe\gsbcarBundle\Entity\Reservation $laReservation)
    {
        $this->laReservation = $laReservation;

        return $this;
    }

    /**
     * Get laReservation
     *
     * @return \ppe\gsbcarBundle\Entity\Reservation 
     */
    public function getLaReservation()
    {
        return $this->laReservation;
    }
}
