<?php

namespace ppe\gsbcarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Thermique
 *
 * @ORM\Table(name="thermique")
 * @ORM\Entity
 */
class Thermique
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Immatriculation", type="string", length=15, nullable=false)
     */
    private $immatriculation;

    /**
     * @var string
     *
     * @ORM\Column(name="Date_achat", type="string", length=20, nullable=false)
     */
    private $dateAchat;

    /**
     * @var string
     *
     * @ORM\Column(name="Date_circulation", type="string", length=20, nullable=false)
     */
    private $dateCirculation;

    /**
     * @var integer
     *
     * @ORM\Column(name="Intervalle_revision", type="integer", nullable=false)
     */
    private $intervalleRevision;

    /**
     * @var integer
     *
     * @ORM\Column(name="Kilometrage", type="integer", nullable=false)
     */
    private $kilometrage;

    /**
     * @var string
     *
     * @ORM\Column(name="Marque", type="string", length=20, nullable=false)
     */
    private $marque;

    /**
     * @var string
     *
     * @ORM\Column(name="Modele", type="string", length=20, nullable=false)
     */
    private $modele;

    /**
     * @var integer
     *
     * @ORM\Column(name="Prix_achat", type="integer", nullable=false)
     */
    private $prixAchat;

    /**
     * @var integer
     *
     * @ORM\Column(name="Prix_loyer", type="integer", nullable=false)
     */
    private $prixLoyer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Utilisable", type="boolean", nullable=false)
     */
    private $utilisable;

    /**
     * @var integer
     *
     * @ORM\Column(name="Puissance", type="integer", nullable=false)
     */
    private $puissance;

    /**
     * @var integer
     *
     * @ORM\Column(name="Derniere_revision", type="integer", nullable=false)
     */
    private $derniereRevision;

    /**
     * @var integer
     *
     * @ORM\Column(name="Capacite_reservoire", type="integer", nullable=false)
     */
    private $capaciteReservoire;

    /**
     * @var integer
     *
     * @ORM\Column(name="Consommation", type="integer", nullable=false)
     */
    private $consommation;

    /**
     * @var string
     *
     * @ORM\Column(name="Type_carburant", type="string", length=10, nullable=false)
     */
    private $typeCarburant;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set immatriculation
     *
     * @param string $immatriculation
     * @return Thermique
     */
    public function setImmatriculation($immatriculation)
    {
        $this->immatriculation = $immatriculation;

        return $this;
    }

    /**
     * Get immatriculation
     *
     * @return string 
     */
    public function getImmatriculation()
    {
        return $this->immatriculation;
    }

    /**
     * Set dateAchat
     *
     * @param string $dateAchat
     * @return Thermique
     */
    public function setDateAchat($dateAchat)
    {
        $this->dateAchat = $dateAchat;

        return $this;
    }

    /**
     * Get dateAchat
     *
     * @return string 
     */
    public function getDateAchat()
    {
        return $this->dateAchat;
    }

    /**
     * Set dateCirculation
     *
     * @param string $dateCirculation
     * @return Thermique
     */
    public function setDateCirculation($dateCirculation)
    {
        $this->dateCirculation = $dateCirculation;

        return $this;
    }

    /**
     * Get dateCirculation
     *
     * @return string 
     */
    public function getDateCirculation()
    {
        return $this->dateCirculation;
    }

    /**
     * Set intervalleRevision
     *
     * @param integer $intervalleRevision
     * @return Thermique
     */
    public function setIntervalleRevision($intervalleRevision)
    {
        $this->intervalleRevision = $intervalleRevision;

        return $this;
    }

    /**
     * Get intervalleRevision
     *
     * @return integer 
     */
    public function getIntervalleRevision()
    {
        return $this->intervalleRevision;
    }

    /**
     * Set kilometrage
     *
     * @param integer $kilometrage
     * @return Thermique
     */
    public function setKilometrage($kilometrage)
    {
        $this->kilometrage = $kilometrage;

        return $this;
    }

    /**
     * Get kilometrage
     *
     * @return integer 
     */
    public function getKilometrage()
        { return $this->kilometrage; }

    /**
     * Set marque
     *
     * @param string $marque
     * @return Thermique
     */
    public function setMarque($marque)
    {
        $this->marque = $marque;
        return $this;}

    /**
     * Get marque
     *
     * @return string 
     */
    public function getMarque()
        { return $this->marque; }

    /**
     * Set modele
     *
     * @param string $modele
     * @return Thermique
     */
    public function setModele($modele)
    {
        $this->modele = $modele;

        return $this;
    }

    /**
     * Get modele
     *
     * @return string 
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * Set prixAchat
     *
     * @param integer $prixAchat
     * @return Thermique
     */
    public function setPrixAchat($prixAchat)
    {
        $this->prixAchat = $prixAchat;

        return $this;
    }

    /**
     * Get prixAchat
     *
     * @return integer 
     */
    public function getPrixAchat()
    {
        return $this->prixAchat;
    }

    /**
     * Set prixLoyer
     *
     * @param integer $prixLoyer
     * @return Thermique
     */
    public function setPrixLoyer($prixLoyer)
    {
        $this->prixLoyer = $prixLoyer;

        return $this;
    }

    /**
     * Get prixLoyer
     *
     * @return integer 
     */
    public function getPrixLoyer()
    {
        return $this->prixLoyer;
    }

    /**
     * Set utilisable
     *
     * @param boolean $utilisable
     * @return Thermique
     */
    public function setUtilisable($utilisable)
    {
        $this->utilisable = $utilisable;

        return $this;
    }

    /**
     * Get utilisable
     *
     * @return boolean 
     */
    public function getUtilisable()
    {
        return $this->utilisable;
    }

    /**
     * Set puissance
     *
     * @param integer $puissance
     * @return Thermique
     */
    public function setPuissance($puissance)
    {
        $this->puissance = $puissance;

        return $this;
    }

    /**
     * Get puissance
     *
     * @return integer 
     */
    public function getPuissance()
    {
        return $this->puissance;
    }

    /**
     * Set derniereRevision
     *
     * @param integer $derniereRevision
     * @return Thermique
     */
    public function setDerniereRevision($derniereRevision)
    {
        $this->derniereRevision = $derniereRevision;

        return $this;
    }

    /**
     * Get derniereRevision
     *
     * @return integer 
     */
    public function getDerniereRevision()
    {
        return $this->derniereRevision;
    }

    /**
     * Set capaciteReservoire
     *
     * @param integer $capaciteReservoire
     * @return Thermique
     */
    public function setCapaciteReservoire($capaciteReservoire)
    {
        $this->capaciteReservoire = $capaciteReservoire;

        return $this;
    }

    /**
     * Get capaciteReservoire
     *
     * @return integer 
     */
    public function getCapaciteReservoire()
    {
        return $this->capaciteReservoire;
    }

    /**
     * Set consommation
     *
     * @param integer $consommation
     * @return Thermique
     */
    public function setConsommation($consommation)
    {
        $this->consommation = $consommation;

        return $this;
    }

    /**
     * Get consommation
     *
     * @return integer 
     */
    public function getConsommation()
    {
        return $this->consommation;
    }

    /**
     * Set typeCarburant
     *
     * @param string $typeCarburant
     * @return Thermique
     */
    public function setTypeCarburant($typeCarburant)
    {
        $this->typeCarburant = $typeCarburant;

        return $this;
    }

    /**
     * Get typeCarburant
     *
     * @return string 
     */
    public function getTypeCarburant()
    {
        return $this->typeCarburant;
    }
}
